package com.sachin.Entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="movieinfo")
public class MovieMySelf {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="movieid")
	private String movieId;
	@Column(name="moviename")
	private String moviename;
	@Column(name="Description")
	private String desc;
	public MovieMySelf(Integer id, String movieId, String moviename, String desc) {
		super();
		this.id = id;
		this.movieId = movieId;
		this.moviename = moviename;
		this.desc = desc;
	}
	public MovieMySelf() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMoviename() {
		return moviename;
	}
	public void setMoviename(String moviename) {
		this.moviename = moviename;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	

}
