package com.sachin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.Entity.MovieMySelf;
import com.sachin.Service.MovieInfoServiceImplementation;
@RestController
@RequestMapping("/movieinfo")
public class movieInfoController {
	
	@Autowired
	MovieInfoServiceImplementation movieinfoimple;
	@PostMapping(value="/save")
	public MovieMySelf save(@RequestBody MovieMySelf moviemyself) {
		return movieinfoimple.save(moviemyself);
	}
	@GetMapping(value="/findone/{userid}")
	public MovieMySelf getone(@PathVariable("userid") Integer userid) {
		return movieinfoimple.getone(userid);
	}
	@GetMapping(value="/findall")
	public List<MovieMySelf> getall(){
		return movieinfoimple.getall();
	}
	@GetMapping(value="/findbymovieid/{movieid}")
	public MovieMySelf findbymovieid(@PathVariable("movieid") String movieid) {
		return movieinfoimple.findByMovieId(movieid);
	}
	

}
