package com.sachin.DAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.sachin.Entity.MovieMySelf;
@Repository
public interface IMovieInfo extends JpaRepository<MovieMySelf, Integer>
{

	MovieMySelf  findByMovieId(String movieid);
	
}
