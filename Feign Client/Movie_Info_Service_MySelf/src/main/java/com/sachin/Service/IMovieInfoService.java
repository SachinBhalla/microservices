package com.sachin.Service;

import java.util.List;

import com.sachin.Entity.MovieMySelf;

public interface IMovieInfoService {
	
	MovieMySelf save(MovieMySelf moviemyself);
	
	MovieMySelf getone(Integer userid);
	
	List<MovieMySelf> getall();
	
	MovieMySelf  findByMovieId(String movieid);
	
	//public User findByUserName(String userName);

}
