package com.sachin.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sachin.DAO.IMovieInfo;
import com.sachin.Entity.MovieMySelf;
@Service
public class MovieInfoServiceImplementation  implements IMovieInfoService
{
	@Autowired
	IMovieInfo movieinfo;

	@Override
	public MovieMySelf save(MovieMySelf moviemyself) {
		return movieinfo.save(moviemyself);
	}

	@Override
	public MovieMySelf getone(Integer userid) {
	   
		MovieMySelf onemovie=movieinfo.findById(userid).orElse(new MovieMySelf());
		return onemovie;
	}

	@Override
	public List<MovieMySelf> getall() {
		List<MovieMySelf> allmovie=movieinfo.findAll();
		return allmovie;
	}

	@Override
	public MovieMySelf findByMovieId(String movieid) {
		return movieinfo.findByMovieId(movieid);
		
	}

}
