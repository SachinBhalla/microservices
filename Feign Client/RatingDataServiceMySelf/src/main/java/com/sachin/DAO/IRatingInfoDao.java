package com.sachin.DAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.sachin.Entity.RatingMySelf;
@Repository
public interface IRatingInfoDao extends JpaRepository<RatingMySelf, Integer>
{
    RatingMySelf  findByMovieId(String movieid);
	
}
