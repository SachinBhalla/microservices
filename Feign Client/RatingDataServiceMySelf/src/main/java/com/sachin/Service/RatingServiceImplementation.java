package com.sachin.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sachin.DAO.IRatingInfoDao;
import com.sachin.Entity.RatingMySelf;


@Service
public class RatingServiceImplementation implements IRatingService 
{
	@Autowired
	IRatingInfoDao ratingdao;

	@Override
	public RatingMySelf save(RatingMySelf rating) {
	return ratingdao.save(rating);
	}

	@Override
	public RatingMySelf getone(Integer userid) {
	return ratingdao.findById(userid).orElse(new RatingMySelf());
	}

	@Override
	public List<RatingMySelf> getall() {
		return ratingdao.findAll();
	}

	@Override
	public RatingMySelf findByMovieId(String movieid) {
		return ratingdao.findByMovieId(movieid);
	}
	

}
