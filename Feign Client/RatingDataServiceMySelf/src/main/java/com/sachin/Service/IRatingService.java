package com.sachin.Service;
import java.util.List;
import com.sachin.Entity.RatingMySelf;

public interface IRatingService {
	
	RatingMySelf save(RatingMySelf rating);
	
	RatingMySelf getone(Integer userid);
	
	List<RatingMySelf> getall();
	
	RatingMySelf  findByMovieId(String movieid);
	
	//public User findByUserName(String userName);

}
