package com.sachin.Entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="ratinginfo")
public class RatingMySelf {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="movieid")
	private String movieId;
	@Column(name="rating")
	int rating;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public RatingMySelf(Integer id, String movieId, int rating) {
		super();
		this.id = id;
		this.movieId = movieId;
		this.rating = rating;
	}
	public RatingMySelf() {
		super();
	}
	
	

}
