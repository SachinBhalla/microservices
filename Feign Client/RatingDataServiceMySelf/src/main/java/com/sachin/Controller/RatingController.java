package com.sachin.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.Entity.RatingMySelf;
import com.sachin.Service.RatingServiceImplementation;

@RestController
@RequestMapping("/rating")
public class RatingController
{
    @Autowired
    RatingServiceImplementation ratingserviceimple;
    
    @PostMapping(value="/save")
	public RatingMySelf save(@RequestBody RatingMySelf ratingmyself) {
		return ratingserviceimple.save(ratingmyself);
	}
    @GetMapping(value="/findone/{userid}")
	public RatingMySelf getone(@PathVariable("userid") Integer userid) {
		return ratingserviceimple.getone(userid);
	}
    @GetMapping(value="/findall")
	public List<RatingMySelf> getall(){
		return ratingserviceimple.getall();
	}
    @GetMapping(value="/findbymovieid/{movieid}")
	public RatingMySelf findbymovieid(@PathVariable("movieid") String movieid) {
		return ratingserviceimple.findByMovieId(movieid);
	}
}


