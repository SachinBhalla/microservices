package com.sachin.InterfacesFeign;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.sachin.Entity.MovieMySelf;
@FeignClient(name = "Moiveinfoservice")
public interface IMovie_Info_Service_MySelf 
{
	//@PostMapping(value = "/user/getAccountsOutletsByMobilesEmailsAndSecondarIds2")
	
	
	@GetMapping(value="/movieinfo/findone/{userid}")
	public MovieMySelf getone(@PathVariable("userid") Integer userid);
	
	@GetMapping(value="/movieinfo/findall")
	public List<MovieMySelf> getall();
	
	@GetMapping(value="/movieinfo/findbymovieid/{movieid}")
	public MovieMySelf findbymovieid(@PathVariable("movieid") String movieid);
	
	/*
	 * @GetMapping(value = "/company-settings/get/company-setting")
	public CompanySettingDTO getCompanySetting();
	 */

}
