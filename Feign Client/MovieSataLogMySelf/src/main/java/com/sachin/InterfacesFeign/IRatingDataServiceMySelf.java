package com.sachin.InterfacesFeign;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.sachin.Entity.RatingMySelf;
@FeignClient(name = "Ratingdataservice")
public interface IRatingDataServiceMySelf 
{
// "/user/getAccountsOutletsByMobilesEmailsAndSecondarIds2"
	  @PostMapping(value="/rating/save")
	  public RatingMySelf save(@RequestBody RatingMySelf ratingmyself);
	  
	  @GetMapping(value="/rating/findone/{userid}")
		public RatingMySelf getone(@PathVariable("userid") Integer userid);
	  
	  @GetMapping(value="/rating/sfindall")
		public List<RatingMySelf> getall();
	  
	  @GetMapping(value="/rating/findbymovieid/{movieid}")
		public RatingMySelf findbymovieid(@PathVariable("movieid") String movieid);
	  
	  

}
