package com.sachin.Entity;

public class MovieMySelf {
	private Integer id;
	private String movieId;
	private String moviename;
	
	private String desc;
	public MovieMySelf(Integer id, String movieId, String moviename, String desc) {
		super();
		this.id = id;
		this.movieId = movieId;
		this.moviename = moviename;
		this.desc = desc;
	}
	public MovieMySelf() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public String getMoviename() {
		return moviename;
	}
	public void setMoviename(String moviename) {
		this.moviename = moviename;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	

}
