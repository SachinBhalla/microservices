package com.sachin.Entity;

public class RatingMySelf {
	private Integer id;
	private String movieId;
	int rating;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMovieId() {
		return movieId;
	}
	public void setMovieId(String movieId) {
		this.movieId = movieId;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public RatingMySelf(Integer id, String movieId, int rating) {
		super();
		this.id = id;
		this.movieId = movieId;
		this.rating = rating;
	}
	public RatingMySelf() {
		super();
	}
	
	

}
