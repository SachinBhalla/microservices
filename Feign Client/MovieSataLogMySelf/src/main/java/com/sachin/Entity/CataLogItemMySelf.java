package com.sachin.Entity;


public class CataLogItemMySelf {
	private String name;
	private String Desc;
	private int rating;
	public CataLogItemMySelf( String name, String desc, int rating) {
		super();
		this.name = name;
		Desc = desc;
		this.rating = rating;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public CataLogItemMySelf() {
		super();
	}
	
	

}
