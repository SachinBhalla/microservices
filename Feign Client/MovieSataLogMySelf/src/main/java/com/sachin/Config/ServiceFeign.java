package com.sachin.Config;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@Configuration
@EnableFeignClients(basePackages = "com.sachin")
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
@SpringCloudApplication
public class ServiceFeign {
	
	

}
