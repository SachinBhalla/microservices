package com.sachin.Controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sachin.Entity.CataLogItemMySelf;
import com.sachin.Entity.MovieMySelf;
import com.sachin.Entity.RatingMySelf;
import com.sachin.InterfacesFeign.IMovie_Info_Service_MySelf;
import com.sachin.InterfacesFeign.IRatingDataServiceMySelf;
@RestController
@RequestMapping("catalogg")
public class CataLogController 
{
	@Autowired
	IMovie_Info_Service_MySelf movieinfo;
	
	@Autowired
	IRatingDataServiceMySelf ratingdata;
	
	
	@GetMapping(value="/findbymovieid/{userid}")
	public CataLogItemMySelf getcatalog(@PathVariable("userid") String userid)
	{
		MovieMySelf movie=movieinfo.findbymovieid(userid);
		//System.out.println("Thanks");
		RatingMySelf rating=ratingdata.findbymovieid(userid);
		return new CataLogItemMySelf(movie.getMoviename(),movie.getDesc(),rating.getRating());
	}
	@GetMapping(value="/checking/{movieid}")
	public MovieMySelf getmovie(@PathVariable("movieid")String movieid)
	{
		return movieinfo.findbymovieid(movieid);
	}
	@GetMapping(value="/checkingrating/{movieid}")
	public RatingMySelf getrating(@PathVariable("movieid")String movieid)
	{
		return ratingdata.findbymovieid(movieid);
	}
	
	
	@GetMapping(value="/findallmoviedata")
	public List<CataLogItemMySelf> getallmovie()
	{
		List<RatingMySelf> ratings=ratingdata.getall();
		List<MovieMySelf> movies=movieinfo.getall();
		
		for(RatingMySelf ratingmy:ratings)
		{
			
		}
		
	    return null;
	}
	

}
