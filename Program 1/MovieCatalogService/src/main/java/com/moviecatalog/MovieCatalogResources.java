package com.moviecatalog;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResources 
{
	@Autowired
	private RestTemplate rest;
	
	@GetMapping("/{userid}")
	public List<CatalogItem>getcatalog(@PathVariable("userid") String userid)
	{
		// get all rating movie
		UserRating ratings=rest.getForObject("http://localhost:8083/rating/user/"+userid, UserRating.class);
		// for each movie id call movie info service and get details
		return ratings.getUserrating().stream().map(rating ->
		{
			Movie movie=rest.getForObject("http://localhost:8082/movies/"+rating.getMovieid(), Movie.class);
			// Take The Movie Id And Call The Movie Information Api With That Movie Id
			return new CatalogItem(movie.getName(),"test",rating.getRating());
			
		})
				.collect(Collectors.toList());
		// put them all together
		
		
		
		
	}
	@GetMapping("/hello")
	public String hello()
	{
		return "hello";
	}
}
