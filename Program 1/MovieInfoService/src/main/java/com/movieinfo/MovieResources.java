package com.movieinfo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("movies")
public class MovieResources 
{
	@GetMapping("/{movieid}")
	public Movie getMovieInfo(@PathVariable("movieid")String movieid)
	{
		return new Movie(movieid,"test");
	}
	

}
