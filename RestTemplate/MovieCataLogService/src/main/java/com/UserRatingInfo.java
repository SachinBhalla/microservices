package com;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class UserRatingInfo 
{
	@Autowired
	private RestTemplate rest;
	
	@HystrixCommand(fallbackMethod = "getfallbackuserrating")
	/*
	 * how to set command property in case of hysteric we can see in the picture
	 */
	public UserRating getuserrating(@PathVariable("userid") String userid)
	{
		return rest.getForObject("http://Rating_Data_Service/rating/user/"+userid, UserRating.class);
		
	}
	
	public UserRating getfallbackuserrating(@PathVariable("userid") String userid)
	{
		UserRating userrating=new UserRating();
		userrating.setUserrating(Arrays.asList(new Rating("0",0)));
		 return userrating;
		
	}
	

}
