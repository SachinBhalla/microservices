package com;

import java.util.List;

public class UserRating 
{
	private List<Rating> userrating;

	public UserRating(List<Rating> userrating) {
		super();
		this.userrating = userrating;
	}

	public List<Rating> getUserrating() {
		return userrating;
	}

	public UserRating() {
		super();
	}

	public void setUserrating(List<Rating> userrating) {
		this.userrating = userrating;
	}

}
