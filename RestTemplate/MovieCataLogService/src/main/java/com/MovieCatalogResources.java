package com;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResources 
{
//	@Autowired
//	private DiscoveryClient dicoveryclient;
	/* This Think Is Optional This Think Only be used If Tried To Get More Control on 
	 * On The Microservices  This Think Only Be Used if Same Microservice Will Be Deployed
	 * On The  The Diferrnts Ports Like 8085,8088 Etc(Discovery Client)
	 */
	
	
	@Autowired
	private RestTemplate rest;
	
	@Autowired
	MovieInfo movieinfo;
	
	@Autowired
	UserRatingInfo userratinginfo;
	
	@GetMapping("/{userid}")
//@HystrixCommand(fallbackMethod = "getfallbackcatalog")
	public List<CatalogItem>getcatalog(@PathVariable("userid") String userid)
	{
//		// get all rating movie
//		UserRating ratings=rest.getForObject("http://Rating_Data_Service/rating/user/"+userid, UserRating.class);
//		// for each movie id call movie info service and get details
//		return ratings.getUserrating().stream().map(rating ->
//		{
//			Movie movie=rest.getForObject("http://Moive_Info_Service/movies/"+rating.getMovieid(), Movie.class);
//			// Take The Movie Id And Call The Movie Information Api With That Movie Id
//			return new CatalogItem(movie.getName(),"test",rating.getRating());
//			
//		})
//				.collect(Collectors.toList());
//		// put them all together
		
		
		/*
		
		UserRating userrating=getuserrating(userid);
	  return  userrating.getUserrating().stream()
			  .map(rating ->getcatalogitem(rating))
			  .collect(Collectors.toList());
			  */
		
		UserRating userrating=userratinginfo.getuserrating(userid);
		  return  userrating.getUserrating().stream()
				  .map(rating ->movieinfo.getcatalogitem(rating))
				  .collect(Collectors.toList());
		
	}
	

//	public List<CatalogItem>getfallbackcatalog(@PathVariable("userid") String userid)
//	{
//		return Arrays.asList(new CatalogItem("NoMovie","",0));
//	}
	/*
	@HystrixCommand(fallbackMethod = "getfallbackcatalogitem")
	private CatalogItem getcatalogitem(Rating rating)
	{
		Movie movie=rest.getForObject("http://Moive_Info_Service/movies/"+rating.getMovieid(), Movie.class);
        return new CatalogItem(movie.getName(),"test",rating.getRating());
	}
	
	private CatalogItem getfallbackcatalogitem(Rating rating)
	{
		return new CatalogItem("Movie Name Not Found","test",rating.getRating());
	}
	
	@HystrixCommand(fallbackMethod = "getfallbackuserrating")
	private UserRating getuserrating(@PathVariable("userid") String userid)
	{
		return rest.getForObject("http://Rating_Data_Service/rating/user/"+userid, UserRating.class);
		
	}
	
	private UserRating getfallbackuserrating(@PathVariable("userid") String userid)
	{
		UserRating userrating=new UserRating();
		userrating.setUserrating(Arrays.asList(new Rating("0",0)));
		 return userrating;
		
	}
	
	
	
	
	
	
	@GetMapping("/hello")
	public String hello()
	{
		return "hello";
	}
	*/
}
