package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class MovieInfo 
{
	@Autowired
	private RestTemplate rest;
	
	
	@HystrixCommand(fallbackMethod = "getfallbackcatalogitem")
	public CatalogItem getcatalogitem(Rating rating)
	{
		Movie movie=rest.getForObject("http://Moive_Info_Service/movies/"+rating.getMovieid(), Movie.class);
        return new CatalogItem(movie.getName(),"test",rating.getRating());
	}
	
	public CatalogItem getfallbackcatalogitem(Rating rating)
	{
		return new CatalogItem("Movie Name Not Found","test",rating.getRating());
	}
	

}
