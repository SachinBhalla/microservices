package com;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rating")
public class MyController 
{
	@GetMapping("/{movieid}")
	public Rating getRating(@PathVariable("movieid") String movieid)
	{
		return  new Rating(movieid,4);
		
	}
	@GetMapping("user/{userid}")
	public UserRating getuserrating(@PathVariable("userid") String userid)
	{
		List<Rating> ratings=Arrays.asList(new Rating("1234",4),
				  							new Rating("5678",5));
		 
		UserRating userrating=new UserRating();
		userrating.setUserrating(ratings);
		return userrating;
	}
	

}
