package com;

import java.util.List;

public class UserRating 
{
	private List<Rating> userrating;

	public UserRating(List<Rating> userrating) {
		super();
		this.userrating = userrating;
	}
	public UserRating() {
		super();
	}

	public List<Rating> getUserrating() {
		return userrating;
	}

	

	public void setUserrating(List<Rating> userrating) {
		this.userrating = userrating;
	}

}
