package com.sachin;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class MyApplication 
{
	@Autowired
	private DbSeeting dbseeting;
	
	@Value("some static value")
	private String value;
	
	@Value("${my.greeting:default value}")
	private String greeting;
	
	@Value("${my.list.value}")
	private List<String>al;
	
	@GetMapping(value="/home")
	public String home()
	{
		return dbseeting.getDbhost()+dbseeting.getName();
	}
	@GetMapping(value="/home2")
	public String home2()
	{
		return value;
	}
	@GetMapping(value="/home3")
	public List<String> home3()
	{
		return al;
	}
	

}
