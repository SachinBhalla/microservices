package com.sachin;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
@Configuration
@ConfigurationProperties("db")
public class DbSeeting 
{
	private String host;
	private String name;
	public String getDbhost() 
	{
		return host;
	}
	public void setDbhost(String host) 
	{
		this.host = host;
	}
	public String getName() 
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}

}
